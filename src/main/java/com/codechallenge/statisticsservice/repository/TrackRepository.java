package com.codechallenge.statisticsservice.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import com.codechallenge.statisticsservice.model.Track;
import com.codechallenge.statisticsservice.model.TrackKey;


@Repository
public interface TrackRepository extends CassandraRepository<Track, TrackKey> {
	
	@Query("select * from track_columnfamily where id = ?0 and event_type=?1")
	List<Track> findByKeyDeliveryId(final UUID deliveryId, String type);
	
	@Query("select * from track_columnfamily where id = ?0 and event_type=?1")
	List<Track> findByKeyClickId(final UUID clickId, String type);
	
}
