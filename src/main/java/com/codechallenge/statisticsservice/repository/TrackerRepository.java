package com.codechallenge.statisticsservice.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

import com.codechallenge.statisticsservice.model.Tracker;


@Repository
public interface TrackerRepository extends CassandraRepository<Tracker, String> {
	
	@Query("select event_type, count(event_type) AS sum from tracker_columnfamily where event_type IN('View', 'Click', 'Install') and event_time>=?0 and event_time<=?1 group by event_type")
	List<Tracker> findStatisticsDetails(Date start, Date end);
	
	@Query("select * from tracker_columnfamily where event_type IN('View', 'Click', 'Install') and event_time>=?0 and event_time<=?1")
	List<Tracker> findStatisticsRecords(Date start, Date end);

}
