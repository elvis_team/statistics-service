package com.codechallenge.statisticsservice.model;

import java.util.Date;
import java.util.UUID;

import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("tracker_columnfamily")
public class Tracker {

	@PrimaryKeyColumn(name = "event_type", type = PrimaryKeyType.PARTITIONED)
	private String type;

	@PrimaryKeyColumn(name = "event_time", ordinal = 0, ordering = Ordering.DESCENDING)
	private Date time;

	@PrimaryKeyColumn(ordinal = 1)
	private String browser;
  
	@PrimaryKeyColumn(name = "operating_system", ordinal = 2)
	private String os;
	
	@PrimaryKeyColumn(ordinal = 3)
	private UUID id;

	@Column("advertisement_id")
	private Integer advertiseId;
  
  	private String site;
  	
  	@Column("timestamp")
	private Date tyme;
  	
//  	@Transient
  	private Integer sum;

	public Tracker() {
	}

	// Constructor added for testing purpose
	public Tracker(String type, String browser, String os, Integer sum) {
		this.type = type;
		this.sum = sum;
		this.browser = browser;
		this.os = os;
	}

	public String getSite() {
		return site;
	}
	
	public void setSite(String site) {
		this.site = site;
	}

	public Integer getAdvertiseId() {
		return advertiseId;
	}

	public void setAdvertiseId(Integer advertiseId) {
		this.advertiseId = advertiseId;
	}

	public Date getTyme() {
		return tyme;
	}

	public void setTyme(Date tyme) {
		this.tyme = tyme;
	}

	@Override
	public String toString() {
		return "Tracker [type=" + type + ", time=" + time + ", browser=" + browser + ", os=" + os + ", id=" + id
				+ ", advertiseId=" + advertiseId + ", site=" + site + ", sum=" + this.sum + ", tyme=" + tyme + "]";
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Integer getSum() {
		return sum;
	}

	public void setSum(Integer sum) {
		this.sum = sum;
	}
	
}
