package com.codechallenge.statisticsservice.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseWrapper {

	private Map<String, Date> interval = null;
	private Map<String, Integer> stats = null;
	private List<Map<String,Map<String,String>>> data = null;
	
	public ResponseWrapper(Map<String, Date> interval, Map<String, Integer> stats,
			List<Map<String,Map<String,String>>> data) {
		this.interval = interval;
		this.stats = stats;
		this.data = data;
	}
	public Map<String, Date> getInterval() {
		return interval;
	}
	public void setInterval(Map<String, Date> interval) {
		this.interval = interval;
	}
	public Map<String, Integer> getStats() {
		return stats;
	}
	public void setStats(Map<String, Integer> stats) {
		this.stats = stats;
	}
	public List<Map<String,Map<String,String>>> getData() {
		return data;
	}
	public void setData(List<Map<String,Map<String,String>>> data) {
		this.data = data;
	}
	
}
