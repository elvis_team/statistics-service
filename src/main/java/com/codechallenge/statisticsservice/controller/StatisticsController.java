package com.codechallenge.statisticsservice.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codechallenge.statisticsservice.exception.RecordNotFoundException;
import com.codechallenge.statisticsservice.model.Track;
import com.codechallenge.statisticsservice.service.StatisticsService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;


@RestController
@RequestMapping("/statistics")
public class StatisticsController {

	private Logger logger = LoggerFactory.getLogger(StatisticsController.class);
	
	@Autowired
	private StatisticsService statisticsService;
	
	@GetMapping(value="/delivery/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	@HystrixCommand(fallbackMethod="fallbackGetDelivery")
	public Map<String, String> getDelivery(@PathVariable("id") UUID id) throws RecordNotFoundException {
		logger.info("Statistics: Getting delivery with id {}...", id);
		Track track = statisticsService.findDeliveryById(id);
		Map<String, String> key = new HashMap<>();
		key.put("key", track.getKey().getId().toString());
		
		return key;
	}
	
	@GetMapping(value="/click/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	@HystrixCommand(fallbackMethod="fallbackGetClick")
	public Map<String, String> getClick(@PathVariable("id") UUID id) throws RecordNotFoundException {
		logger.info("Statistics: Getting click with id {}...", id);
		Track track = statisticsService.findClickById(id);
		Map<String, String> key = new HashMap<>();
		key.put("key", track.getKey().getId().toString());
		
		return key;
	}
	
	@GetMapping(value="/summary", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseWrapper> summary(@RequestParam("start") String start, @RequestParam("end") String end) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		logger.info("Getting summary between {} - {}", start, end);
		Map<String, Date> interval = new HashMap<>();
		interval.put("start", formatter.parse(start.replaceAll(" ", "+")));
		interval.put("end", formatter.parse(end.replaceAll(" ", "+")));
		Map<String, Integer> stats = statisticsService.findStatistics(formatter.parse(start.replaceAll(" ", "+")), formatter.parse(end.replaceAll(" ", "+")));
		ResponseWrapper response = new ResponseWrapper(interval, stats, null);
		
		return new ResponseEntity<ResponseWrapper>(response, HttpStatus.OK);
	}
	
	@GetMapping(value="/summary",  params = {"group_by"}, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseWrapper> summaryByBrowserAndOS(@RequestParam("start") String start, @RequestParam("end") String end, @RequestParam("group_by") String ...group_by) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		logger.info("Getting summary between {} - {}", start, end);
		logger.info("Group count - {}", group_by.length);
		Map<String, Date> interval = new HashMap<>();
		interval.put("start", formatter.parse(start.replaceAll(" ", "+")));
		interval.put("end", formatter.parse(end.replaceAll(" ", "+")));
		List<Map<String,Map<String,String>>> data = statisticsService.findStatistics(formatter.parse(start.replaceAll(" ", "+")), formatter.parse(end.replaceAll(" ", "+")), group_by);
		ResponseWrapper response = new ResponseWrapper(interval, null, data);
		
		return new ResponseEntity<ResponseWrapper>(response, HttpStatus.OK);
	}
	
	public Map<String, String> fallbackGetDelivery(UUID id){
		logger.error("delivery id fallback triggered...{}", id);
		Map<String, String> key = new HashMap<>();
		key.put("key", "404");
		
		return key;
	}
	
	public Map<String, String> fallbackGetClick(UUID id){
		logger.error("click id fallback triggered...{}", id);
		Map<String, String> key = new HashMap<>();
		key.put("key", "404");
		
		return key;
	}
	
}
