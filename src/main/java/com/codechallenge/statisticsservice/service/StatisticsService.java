package com.codechallenge.statisticsservice.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.codechallenge.statisticsservice.exception.RecordNotFoundException;
import com.codechallenge.statisticsservice.model.Track;


public interface StatisticsService {
	
	Track findDeliveryById(UUID deliveryId) throws RecordNotFoundException;

	Track findClickById(UUID clickId) throws RecordNotFoundException;
	
	Map<String, Integer> findStatistics(Date starttime, Date endtime);
	
	List<Map<String,Map<String,String>>> findStatistics(Date starttime, Date endtime, String ...category);

}
