package com.codechallenge.statisticsservice.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codechallenge.statisticsservice.exception.RecordNotFoundException;
import com.codechallenge.statisticsservice.model.Track;
import com.codechallenge.statisticsservice.model.Tracker;
import com.codechallenge.statisticsservice.repository.TrackRepository;
import com.codechallenge.statisticsservice.repository.TrackerRepository;


@Service
public class StatisticsServiceImpl implements StatisticsService {

	private static org.slf4j.Logger LOG = LoggerFactory.getLogger(StatisticsServiceImpl.class);

	@Autowired
	TrackRepository trackRepository;
	
	@Autowired
	TrackerRepository trackerRepository;
	

	@Override
	public Track findDeliveryById(UUID deliveryId) throws RecordNotFoundException {
		LOG.info("...Getting delivery with Id: {}", deliveryId);
		List<Track> track = trackRepository.findByKeyDeliveryId(deliveryId, "View");
		if(track.size() == 0)
			throw new RecordNotFoundException("Oops! Could not find entity with id:" + deliveryId);
		return track.get(0);
	}

	@Override
	public Track findClickById(UUID clickId) throws RecordNotFoundException {
		LOG.info("...Getting click with Id: {}", clickId);
		List<Track> track = trackRepository.findByKeyClickId(clickId, "Click");
		LOG.info("...Click is: {}", track);
		if(track.size() == 0)
			throw new RecordNotFoundException("Oops! Could not find entity with id:" + clickId);
		return track.get(0);
	}

	@Override
	public Map<String, Integer> findStatistics(Date starttime, Date endtime) {
		LOG.info("...Getting aggregate statistics");
		List<Tracker> tracker = trackerRepository.findStatisticsDetails(starttime, endtime);
		LOG.info("Fetch count = " + tracker.size());
		Map<String, Integer> stats = new HashMap<>();
		tracker.forEach(t -> {
			String str = null;
			switch(t.getType()) {
			case "View":
				str = "deliveries";
				break;
			case "Click":
				str = "clicks";
				break;
			case "Install":
				str = "installs";
				break;
			}
			stats.put(str, t.getSum());
		});
		return stats;
	}

	@Override
	public List<Map<String,Map<String,String>>> findStatistics(Date starttime, Date endtime, String... category) {
		LOG.info("...Getting aggregate statistics with {} category [{}]", category.length, category);
		List<Tracker> trackers = (List<Tracker>) trackerRepository.findStatisticsRecords(starttime, endtime);
		List<Map<String,Map<String,String>>> data = new ArrayList<>();
		if(category.length == 1) {
			// process block when we have a single category
			 Map<String, List<String>> result =
					 trackers.parallelStream().collect(
		                Collectors.groupingBy(
		                		Tracker::getBrowser,
		                        Collectors.mapping(Tracker::getType, Collectors.toList()
	                		)
		                )
		     );
			 result.forEach((k,v)->{
				Map<String, String> fcontent = new HashMap<>();
				Map<String, String> scontent = new HashMap<>();
				Map<String, Map<String, String>> fields = new HashMap<>();
				fcontent.put("browser", k);
				fields.put("fields", fcontent);
				Map<String, Long> result1 =
						v.parallelStream().collect(
						 Collectors.groupingBy(
		                        Function.identity(), Collectors.counting()
		                )
				 );
				 scontent.put("deliveries", String.valueOf(result1.get("View")));
				 scontent.put("clicks", String.valueOf(result1.get("Click")));
				 scontent.put("installs", String.valueOf(result1.get("Install")));
				 fields.put("stats", scontent);
				 data.add(fields);
			});
		} else {
			// process block when we have double categories
			 trackers.parallelStream().collect(Collectors.groupingBy(t -> {
	             return new ArrayList<String>(Arrays.asList(t.getBrowser(), t.getOs()));
	         }))
			 .entrySet()
			 .stream()
			 .forEach(t -> {
			         Map<String, String> fcontent = new HashMap<>();
					 Map<String, String> scontent = new HashMap<>();
					 Map<String, Map<String, String>> fields = new HashMap<>();
					 fcontent.put("browser", t.getKey().get(0));
					 fcontent.put("os", t.getKey().get(1));
					 fields.put("fields", fcontent);
			         t.getValue().parallelStream().collect(
				         Collectors.groupingBy(
								Tracker::getType,
								Collectors.counting()
							)
					 )
		         	.forEach((k,v)->{
		         		String str = null;
		    			switch(k) {
		    			case "View":
		    				str = "deliveries";
		    				break;
		    			case "Click":
		    				str = "clicks";
		    				break;
		    			case "Install":
		    				str = "installs";
		    				break;
		    			}
		         		scontent.put(str, String.valueOf(v));
		         	});
		         	fields.put("stats", scontent);
		         	data.add(fields);
			     });
		}
		return data;
	}

}
