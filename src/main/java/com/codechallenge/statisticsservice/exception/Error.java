package com.codechallenge.statisticsservice.exception;

public class Error {
	private String message;
	private String reason;
	private String path;
	private String error;
	private Integer status;
	private String timestamp;
	
	public Error(String message, String reason, String path, String error, Integer status, String timestamp) {
		this.message = message;
		this.reason = reason;
		this.path = path;
		this.error = error;
		this.status = status;
		this.timestamp = timestamp;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "Error [message=" + message + ", reason=" + reason + ", path=" + path + ", error=" + error + ", status="
				+ status + ", timestamp=" + timestamp + "]";
	}
}
