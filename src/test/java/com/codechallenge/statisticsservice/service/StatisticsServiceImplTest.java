package com.codechallenge.statisticsservice.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.validation.ValidationException;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.codechallenge.statisticsservice.StatisticsServiceApplication;
import com.codechallenge.statisticsservice.exception.RecordNotFoundException;
import com.codechallenge.statisticsservice.model.Track;
import com.codechallenge.statisticsservice.model.TrackKey;
import com.codechallenge.statisticsservice.model.Tracker;
import com.codechallenge.statisticsservice.repository.TrackRepository;
import com.codechallenge.statisticsservice.repository.TrackerRepository;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = StatisticsServiceApplication.class)
public class StatisticsServiceImplTest {

    @Mock
	TrackRepository trackRepository;
	
    @Mock
	TrackerRepository trackerRepository;

    @InjectMocks
    private StatisticsServiceImpl statisticsServiceImpl;
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    List<Track> trackA = new ArrayList<>();
    List<Track> trackB = new ArrayList<>();
    List<Track> trackC = new ArrayList<>();
    List<Tracker> trackersA = new ArrayList<>();
    List<Tracker> trackersB = new ArrayList<>();
    
    @Before
    public void setUp() {
    	TrackKey trackey1 = new TrackKey("Views", UUID.fromString("244cf0db-ba28-4c5f-8c9c-2bf11ee42988"), new Date());
    	TrackKey trackey2 = new TrackKey("Views", UUID.fromString("fff54b83-49ff-476f-8bfb-2ec22b252c32"), new Date());
        trackA.add(new Track(trackey1, new Date()));
        trackB.add(new Track(trackey2, new Date()));
        
        trackersA.add(new Tracker("View", "Chrome", "BLACK", 13));
		trackersA.add(new Tracker("Install", "Opera", "IOS", 3));
		trackersA.add(new Tracker("Click", "Opera", "BLACK", 41));
		
		trackersB.add(new Tracker("Click", "Moz", "BLACK", 1));
		trackersB.add(new Tracker("Install", "Opera", "IOS", 1));
		trackersB.add(new Tracker("Install", "Chrome", "BLACK", 1));
		trackersB.add(new Tracker("Click", "Chrome", "Android", 1));
		trackersB.add(new Tracker("View", "Moz", "BLACK", 1));
		trackersB.add(new Tracker("Click", "Opera", "IOS", 1));
		trackersB.add(new Tracker("Install", "Opera", "IOS", 1));
		trackersB.add(new Tracker("Click", "Chrome", "Android", 1));
		trackersB.add(new Tracker("Install", "Opera", "BLACK", 1));
		trackersB.add(new Tracker("Click", "Moz", "BLACK", 1));
		trackersB.add(new Tracker("View", "Opera", "IOS", 1));
		trackersB.add(new Tracker("Click", "Moz", "Android", 1));
		trackersB.add(new Tracker("Click", "Chrome", "IOS", 1));
		trackersB.add(new Tracker("View", "Moz", "IOS", 1));
		trackersB.add(new Tracker("Install", "Chrome", "Android", 1));
		trackersB.add(new Tracker("View", "Moz", "IOS", 1));
		trackersB.add(new Tracker("Install", "Chrome", "BLACK", 1));
		trackersB.add(new Tracker("View", "Chrome", "IOS", 1));
		trackersB.add(new Tracker("View", "Chrome", "BLACK", 1));
		trackersB.add(new Tracker("Install", "Opera", "IOS", 1));
		trackersB.add(new Tracker("View", "Chrome", "Android", 1));
		trackersB.add(new Tracker("Install", "Chrome", "BLACK", 1));
		trackersB.add(new Tracker("View", "Moz", "IOS", 1));
		trackersB.add(new Tracker("Click", "Opera", "BLACK", 1));
		trackersB.add(new Tracker("View", "Moz", "IOS", 1));
		trackersB.add(new Tracker("Install", "Opera", "IOS", 1));
		trackersB.add(new Tracker("Install", "Chrome", "BLACK", 1));
		trackersB.add(new Tracker("Click", "Moz", "Android", 1));
		trackersB.add(new Tracker("Install", "Opera", "IOS", 1));
		trackersB.add(new Tracker("View", "Moz", "Android", 1));
		trackersB.add(new Tracker("Install", "Opera", "IOS", 1));
    }
    
	@Test
	public void findDeliveryByIdTest() throws RecordNotFoundException {
		when(trackRepository.findByKeyDeliveryId(UUID.fromString("244cf0db-ba28-4c5f-8c9c-2bf11ee42988"), "View")).thenReturn(trackA);
		Track track = statisticsServiceImpl.findDeliveryById(UUID.fromString("244cf0db-ba28-4c5f-8c9c-2bf11ee42988"));
		assertNotNull(track);
		assertEquals(track.getKey().getId(), UUID.fromString("244cf0db-ba28-4c5f-8c9c-2bf11ee42988"));
	}
	
	@Test
	public void findDeliveryByIdNoRecordFoundExceptionTest() throws RecordNotFoundException {
		when(trackRepository.findByKeyDeliveryId(UUID.fromString("244cf0db-ba28-4c5f-8c9c-2bf11ee42900"), "View")).thenReturn(trackC);
		
		thrown.expect(RecordNotFoundException.class);
		thrown.expectMessage(containsString("Oops!"));
		
		statisticsServiceImpl.findDeliveryById(UUID.fromString("244cf0db-ba28-4c5f-8c9c-2bf11ee42900"));
	}
	
	@Test
	public void findClickByIdTest() throws RecordNotFoundException {
		when(trackRepository.findByKeyClickId(UUID.fromString("fff54b83-49ff-476f-8bfb-2ec22b252c32"), "Click")).thenReturn(trackB);
		Track track = statisticsServiceImpl.findClickById(UUID.fromString("fff54b83-49ff-476f-8bfb-2ec22b252c32"));
		assertNotNull(track);
		assertEquals(track.getKey().getId(), UUID.fromString("fff54b83-49ff-476f-8bfb-2ec22b252c32"));
	}
	
	@Test
	public void findClickByIdNoRecordFoundExceptionTest() throws RecordNotFoundException {
		when(trackRepository.findByKeyClickId(UUID.fromString("fff54b83-49ff-476f-8bfb-2ec22b252c31"), "Click")).thenReturn(trackC);
		
		thrown.expect(RecordNotFoundException.class);
		thrown.expectMessage(containsString("Oops!"));
		
		statisticsServiceImpl.findClickById(UUID.fromString("fff54b83-49ff-476f-8bfb-2ec22b252c31"));
	}
	
	@Test
	public void findStatisticsTest() {
		Date starttime = new Date();
		Date endtime = new Date();
		when(trackerRepository.findStatisticsDetails(starttime, endtime)).thenReturn(trackersA);
		
		Map<String, Integer> result = statisticsServiceImpl.findStatistics(starttime, endtime);
		assertEquals(result.get("deliveries").toString(), "13");
		assertEquals(result.get("clicks").toString(), "41");
		assertEquals(result.get("installs").toString(), "3");
	}
	
	@Test
	public void findStatisticsWithSingleGroupTest() {
		Date starttime = new Date();
		Date endtime = new Date();
		String[] group = new String[] {"browser"};
		when(trackerRepository.findStatisticsRecords(starttime, endtime)).thenReturn(trackersB);
		List<Map<String, Map<String, String>>> result = statisticsServiceImpl.findStatistics(starttime, endtime, group);
		assertThat(result.size(), Matchers.is(3));
	}
	
	@Test
	public void findStatisticsWithDoubleGroupTest() {
		Date starttime = new Date();
		Date endtime = new Date();
		String[] group = new String[] {"browser", "os"};
		when(trackerRepository.findStatisticsRecords(starttime, endtime)).thenReturn(trackersB);
		List<Map<String, Map<String, String>>> result = statisticsServiceImpl.findStatistics(starttime, endtime, group);
		assertThat(result.size(), Matchers.is(8));
	}
	
}
